package main;
import java.util.Scanner;

/**
 * Solution to UVA judge problem 11498 - Division of Nlogonia
 * @author Juan Pablo BC
 */
public class Main
{
    public static void main(String[] args)
    {
        int n,cx,cy,x,y;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        while(n!=0)
        {
            cx = sc.nextInt();
            cy = sc.nextInt();
            while((n--)>0)
            {
                x = sc.nextInt();
                y = sc.nextInt();
                if(x==cx || y==cy)
                    System.out.println("divisa");
                else if (x < cx && y > cy)
                    System.out.println("NO");
                else if (x > cx && y > cy)
                    System.out.println("NE");
                else if (x > cx && y < cy)
                    System.out.println("SE");
                else if (x < cx && y < cy)
                    System.out.println("SO");
            }
            n = sc.nextInt();
        }
    }
}
